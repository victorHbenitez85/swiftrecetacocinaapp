//
//  DetailViewController.swift
//  swiftRecetarioDeCocinaApp
//
//  Created by Victor Hugo Benitez Bosques on 16/09/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet var recipeImageView: UIImageView!
    var recipe : Recipe! //Objeto del Modelo que tendra los atributos y parametros pasados desde el segue
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var ratingButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad() //Se configure de manera correcta
        navigationController?.hidesBarsOnSwipe = false //Esconder la barra de navegacion al hacer swipe

        
        self.title = self.recipe.name //Colocamos el nombre de la receta en la barra de navegacion
        self.recipeImageView.image = self.recipe.image //set imagen del modelo a la imageView
        
        self.tableView.backgroundColor = UIColor(red: 1.0, green: 0.3, blue: 0.05, alpha: 0.25) //Change TableView color 255/255 83/255 13/255
        self.tableView.tableFooterView = UIView(frame: CGRect.zero) // despues de la ultima celda Desaparezca buena practica
        self.tableView.separatorColor = UIColor(red: 1.0, green: 0.3, blue: 0.05, alpha: 0.75) //Cambiar el color del separador
        
        /*Celdas auto ajustables al contenido dinamic tipe*/
        self.tableView.estimatedRowHeight = 44.0  //guia en pixeles para el alto por defecto
        self.tableView.rowHeight = UITableViewAutomaticDimension // calcule tamaño de la celda al tamaño de su contenido y lo actualize dinamicamente
        
        let image = UIImage(named: self.recipe.rating) //Creamos la imagen con el valor del atributo del modelo
        self.ratingButton.setImage(image, for: .normal) //cambiamos la imagen


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {  //funcion que se ejecuta al cargar la vista
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = false  //no oculta la barra al hacer swipe
        navigationController?.setNavigationBarHidden(false, animated: true) //Si esta oculta coloca el atributo en false
        
    }
    
    //Recibe la informacion del unwind segue
    @IBAction func close(segue: UIStoryboardSegue){ //recive el segue de ReviewViewDetail
       
        if let reivewVC = segue.source as? ReviewViewController{
            
            if let rating = reivewVC.ratingSelected{
                print(rating)
                self.recipe.rating = rating //Asignacion de la valoracion de l platillo por el usuario
                
                let image = UIImage(named: self.recipe.rating) //Creamos la imagen con el valor del atributo del modelo
                self.ratingButton.setImage(image, for: .normal) //cambiamos la imagen
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailViewController : UITableViewDataSource{ //Requiere utilizar sus metodos por defecto
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3 //Secciones datos estaticos, ingredientes y pasos
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 2  //section 1: 2 filas
        case 1:
            return self.recipe.ingridients.count  //section 2: filas tamaño array ingridients
        case 2:
            return self.recipe.steps.count      //section3: Filas tamaño array steps
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {  //configuracion de la celda
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailRecipeCell", for: indexPath) as! RecipeDetalViewCell //forzamos a usar el tipo de celda que hemos creado
        
        cell.backgroundColor = UIColor.clear //Elimina el color de la celda
        
        switch indexPath.section {  //En la seccion en que se encuentre mostrara en las filas la informacion
        case 0:
            switch indexPath.row { //fila mostrada
            case 0:
                cell.keyLabel.text = "Nombre:"
                cell.valueLabel.text = self.recipe.name
            case 1:
                cell.keyLabel.text = "Tiempo"
                cell.valueLabel.text = "\(self.recipe.time!)" + " min"
            /*case 2:
                cell.keyLabel.text = "Favorita:"
                if self.recipe.isFavorite{
                    cell.valueLabel.text = "Si"
                }else{
                    cell.valueLabel.text = "No"
                }*/
            default:
                break
            }
        case 1:  //Si la section es la 2
            if indexPath.row == 0{
                cell.keyLabel.text = "Ingredientes:"
            }else{
                cell.keyLabel.text = ""
            }
            cell.valueLabel.text = self.recipe.ingridients[indexPath.row] //colocara los ingredientes por fila
        case 2:
            if indexPath.row == 0{
                cell.keyLabel.text = "Pasos:"
            }else{
                cell.keyLabel.text = ""
            }
            cell.valueLabel.text = self.recipe.steps[indexPath.row]  //colocara los pasos en las filas
        default:
            break
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? { //Agrega un titulo a cada seccion
        var title = ""
        
        switch section {  //la seccion en indexPath colocara un Titulo
        case 1:
            title = "Ingredientes"
        case 2:
            title = "Pasos"
        default:
            break
        }
    
        return title
    }
    
    

}

extension DetailViewController : UITableViewDelegate{

}

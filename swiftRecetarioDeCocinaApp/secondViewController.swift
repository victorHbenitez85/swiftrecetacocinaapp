//
//  secondViewController.swift
//  swiftRecetarioDeCocinaApp
//
//  Created by Victor Hugo Benitez Bosques on 14/09/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit

class secondViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    var recipes : [Recipe] = [] //array que contenra objetos del modelo Recipe
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* 
        tableView asgna los metodos a secondViewController, secondViewController es el dataSource
        self.tableView.dataSource = self
        self.tableView.delegate = self*/
        
        
        var recipe = Recipe(name: "Totilla de patatas",
                            image: #imageLiteral(resourceName: "tortilla"),
                            time: 20,
                            ingredients: ["Patatas", "Huevos", "Cebollas"],
                            steps: ["1. Pelar patatas y cebollas", "2. Cortar las patatas y la cebollas", "3. Batir los huevos y cocerlos con el resto"]
        )  //Argumentos nombre e imagen del modelo Recipe
        recipes.append(recipe)
        
        recipe = Recipe(name: "Pizza Margarita",
                        image: #imageLiteral(resourceName: "pizza"),
                        time:40,
                        ingredients: ["Masa", "Salsa", "Carnes frias"],
                        steps: ["1. Colocar la salsa a la masa", "2. Colocar las carnes frias", "3. Colocar en el horno por 30 monitos", "4. Servir y degustar"])
        recipes.append(recipe)
        
        recipe = Recipe(name: "Hamburguesa con queso",
                        image: #imageLiteral(resourceName: "hamburguesa"),
                        time: 30,
                        ingredients: ["Pan para hamburguesa","Carne especial para hamburguesa", "Vegetales al gusto", "Mayonesa"],
                        steps: ["1. Colocal en la plancha la carne para hamburguesa", "2. Lavar las verduras", "3. Colocar la carne en el pan para hamburguesa", "Servir y degustar"]
            
        )
        recipes.append(recipe)
        
        recipe = Recipe(name: "Ensalada Cesar",
                        image: #imageLiteral(resourceName: "ensalada"),
                        time: 15,
                        ingredients: ["Lechuga Romana", "Pechuga de pollo", "Tomates", "Salsa al pesto"],
                        steps: ["1. Dorar la pechuga de pollo en sarten", "2. Preparar la salsa al pesto", "3. lavar la lechuga y secarla", "4. Unir la lechuga con el tomate", "5. Incorporar la pechuga de pollo en turas y un poco de queso parmesano"])
        recipes.append(recipe)
        recipe = Recipe(name: "Tacos a la Mexicana",
                        image: #imageLiteral(resourceName: "tacos"),
                        time: 20,
                        ingredients: ["Tortilla", "Carne", "Verduras al gusto", "Salsa taquera"],
                        steps: ["1. Sofreir la tortilla", "2. Freir la Carne", "3.Agregar las verduras al gusto", "4. Servir la salsa", "5. Degustar"])
        recipes.append(recipe)
        
        recipe = Recipe(name: "Camaron Fresco con salsa de tomate",
                        image: #imageLiteral(resourceName: "camaron"),
                        time: 20,
                        ingredients: ["Camaron", "tomates", "Aceite de oliva"],
                        steps: ["1. Sofreir los camarones con aceite de oliva ", "2. Pelar los tomates y rayarlos", "3. Preparar una vinagreta para acompañar a los camarones", "4. Servir"])
        recipes.append(recipe)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Hide status bar
    override var prefersStatusBarHidden: Bool{
        get{
            return true
        }
    }
    

}

// UITableViewDelegate se encarga de configurar seleccion de un elementos configuracion del header o footer
extension secondViewController: UITableViewDataSource{  // UITableViewDataSource: se encarga de mostrar la informacion cuantas filas, secciones etc. Delegate se encarga mas de la interactividad con las celdas

    // Se esta delegando los metodos de una UITableView  a la UIViewController
    func numberOfSections(in tableView: UITableView) -> Int { //Secciones de la tabla
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {//Numero de filas va a tener la tabla definido por el tamaño del array recipes
        return self.recipes.count //Numero de filas defindo por el tamaño del array
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { //configuracion de las celda, IndexPath definada como la seccion y la fila
        let recipe = recipes[indexPath.row]
        
        //edicion de la celda de la tabla
        let cellID = "RecipeCell" //identificador unico de la celda
        let  cell = self.tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! FullRecipeCell
        
        cell.thumbnailImage.image = recipe.image
        cell.nameLabel.text = recipe.name
        cell.ingredientsLabel.text = "Ingredientes: \(recipe.ingridients.count)"
        
        
        return cell
    }

}

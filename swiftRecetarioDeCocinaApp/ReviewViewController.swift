//
//  ReviewViewController.swift
//  swiftRecetarioDeCocinaApp
//
//  Created by Victor Hugo Benitez Bosques on 20/09/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {
    
    @IBOutlet var ratingStackView: UIStackView!
    @IBOutlet var backgroundImageView: UIImageView!
    
    //botones para edicion
    @IBOutlet var firstButton: UIButton!
    @IBOutlet var secondButton: UIButton!
    @IBOutlet var thirdButton: UIButton!
    
    var ratingSelected : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*Efecto difunimado que se agrega encima de una vista o imagen*/
        let blurEffect = UIBlurEffect(style: .dark) //efecto difuminado
        let blurEffectView = UIVisualEffectView(effect: blurEffect) //Creacion del efcto
        blurEffectView.frame = view.bounds //el efecto tendra el tamaño de la vista como una capa 
        backgroundImageView.addSubview(blurEffectView) //El frame creado con el efecto se agrega a la imagen
        
        let scale = CGAffineTransform(scaleX: 0.0, y: 0.0) //Escalado de la stack view 0 con CGAffineTransform
        let translation = CGAffineTransform(translationX: 0.0, y: 500.0)
        
        //ratingStackView.transform = scale.concatenating(translation) //Combinar escalado con translación
        self.firstButton.transform = scale.concatenating(translation)
        self.secondButton.transform = scale.concatenating(translation)
        self.thirdButton.transform = scale.concatenating(translation)
    }
    
    override func viewDidAppear(_ animated: Bool) { //Se ejecuta cuando se a cargado la vista
        super.viewDidAppear(animated)
        
        /*  Animacion de los botones dentro de la stack view
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: { //handler
                self.ratingStackView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0) //Escala la imagen a la identidad
            }, completion: nil)
       */
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.5, options: [], animations: {
            //self.ratingStackView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0) //Escala la imagen a la identidad
            self.firstButton.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: { (success) in
                
                UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.5, options: [], animations: {
                        self.secondButton.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    }, completion: { (success) in
                        
                        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.5, options: [], animations: {
                            self.thirdButton.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                            }, completion: nil)
                })
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    
    
    
    @IBAction func ratingPressed(_ sender: UIButton) {
        
        
        switch sender.tag { //tag del boton presionado
        case 1:
            ratingSelected = "dislike"
        case 2:
            ratingSelected = "good"
        case 3:
            ratingSelected = "great"
        default:
            break
        }
        
        //forzar regresar a una vista anterior cerrar la ventana
        performSegue(withIdentifier: "unwindToDetalView", sender: sender) //sender : enviado por el boton
        
    }
    
    
    

}

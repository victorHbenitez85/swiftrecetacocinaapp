//
//  Recipe.swift
//  swiftRecetarioDeCocinaApp
//
//  Created by Victor Hugo Benitez Bosques on 14/09/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
// Modelo de datos de la app Recetas de Cocina

import Foundation
import UIKit

class Recipe: NSObject {  //objeto cualquiera que no hereda de nadie
    
    //attributes
    var name : String!
    var image : UIImage!
    var time : Int!
    var ingridients : [String]!  //todos los ingredientes de un array
    var steps : [String]!       //Pasos de eleboracion de la receta
    var rating : String = "rating" //Valoracion del platillo
    //var isFavorite : Bool = false //marca que tendran las recetas favoritas
    
    init(name: String, image: UIImage, time: Int, ingredients: [String], steps: [String]) {  //Solo se creara la instancia añadiendo el nombre del platillo
        self.name = name    //self = this
        self.image = image
        self.time = time
        self.ingridients = ingredients
        self.steps = steps
    }
    
    
    
}

//
//  ViewController.swift
//  swiftRecetarioDeCocinaApp
//
//  Created by Victor Hugo Benitez Bosques on 14/09/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit

/*
 UITableViewController no se necesitan importar los delegado : ser un TableViewController o contener un UITableViewController
 UIViewController necesita ademas las funcionalidades UITableViewDataSource, UITableViewDelegate
 */
class ViewController: UITableViewController {

    var recipes : [Recipe] = [] //array que contenra objetos del modelo Recipe
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil) //eliminamos titulo boton regresar aqui se encuentra agregado el navigation controller
        
        var recipe = Recipe(name: "Tortilla de patatas",
                            image: #imageLiteral(resourceName: "tortilla"),
                            time: 20,
                            ingredients: ["Patatas", "Huevos", "Cebollas"],
                            steps: ["1. Pelar patatas y cebollas", "2. Cortar las patatas y la cebollas", "3. Batir los huevos y cocerlos con el resto"]
                            )  //Argumentos nombre e imagen del modelo Recipe
        recipes.append(recipe)
        
        recipe = Recipe(name: "Pizza Margarita",
                        image: #imageLiteral(resourceName: "pizza"),
                        time:40,
                        ingredients: ["Masa", "Salsa", "Carnes frias"],
                        steps: ["1. Colocar la salsa a la masa", "2. Colocar las carnes frias", "3. Colocar en el horno por 30 monitos", "4. Servir y degustar"])
        recipes.append(recipe)
        
        recipe = Recipe(name: "Hamburguesa con queso",
                        image: #imageLiteral(resourceName: "hamburguesa"),
                        time: 30,
                        ingredients: ["Pan para hamburguesa","Carne especial para hamburguesa", "Vegetales al gusto", "Mayonesa"],
                        steps: ["1. Colocal en la plancha la carne para hamburguesa", "2. Lavar las verduras", "3. Colocar la carne en el pan para hamburguesa", "Servir y degustar"]
        
        )
        recipes.append(recipe)
    
        recipe = Recipe(name: "Ensalada Cesar",
                        image: #imageLiteral(resourceName: "ensalada"),
                        time: 15,
                        ingredients: ["Lechuga Romana", "Pechuga de pollo", "Tomates", "Salsa al pesto"],
                        steps: ["1. Dorar la pechuga de pollo en sarten", "2. Preparar la salsa al pesto", "3. lavar la lechuga y secarla", "4. Unir la lechuga con el tomate", "5. Incorporar la pechuga de pollo en turas y un poco de queso parmesano"])
        recipes.append(recipe)
        recipe = Recipe(name: "Tacos a la Mexicana",
                        image: #imageLiteral(resourceName: "tacos"),
                        time: 20,
                        ingredients: ["Tortilla", "Carne", "Verduras al gusto", "Salsa taquera"],
                        steps: ["1. Sofreir la tortilla", "2. Freir la Carne", "3.Agregar las verduras al gusto", "4. Servir la salsa", "5. Degustar"])
        recipes.append(recipe)
        
        recipe = Recipe(name: "Camaron Fresco con salsa de tomate",
                        image: #imageLiteral(resourceName: "camaron"),
                        time: 20,
                        ingredients: ["Camaron", "tomates", "Aceite de oliva"],
                        steps: ["1. Sofreir los camarones con aceite de oliva ", "2. Pelar los tomates y rayarlos", "3. Preparar una vinagreta para acompañar a los camarones", "4. Servir"])
        recipes.append(recipe)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = true //Esconder la barra de navegacion al hacer swipe
  
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Hide status bar
    override var prefersStatusBarHidden: Bool{
        get{
            return true
        }
    }

    // UITableViewDelegate se encarga de configurar seleccion de un elementos configuracion del header o footer  api UITableViewDelegate

    // MARK:- UITableViewDataSource
    // Se esta delegando los metodos de una UITableView  a la UIViewController
    override func numberOfSections(in tableView: UITableView) -> Int { //Secciones de la tabla
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {//Numero de filas va a tener la tabla definido por el tamaño del array recipes
        return self.recipes.count //Numero de filas defindo por el tamaño del array
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { //configuracion de las celda, IndexPath definada como la seccion y la fila
        let recipe = recipes[indexPath.row]
        
        //edicion de la celda de la tabla
        let cellID = "RecipeCell" //identificador unico de la celda
        
        // Para ser uso de RecipeCell casting a la Celda Editada
        let  cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell //Le inicamos sobre la celda que usara los datos
        cell.thumbnalImageView.image = recipe.image
        cell.nameLabel.text = recipe.name
        cell.timeLabel.text = "\(recipe.time!) min"
        cell.ingredientsLabel.text = "Ingredientes: \(recipe.ingridients.count)"
        
        /* Editar las imagenes de las celdas
        cell.thumbnalImageView.layer.cornerRadius = 42.0 //counstruimos una capa circular
        cell.thumbnalImageView.clipsToBounds = true //Recorta la imagen del circulo creado */
        
        //TableViewCell tiene opciones de seleccion de celda usando el accesory (modo grafico) y accessoryType
        /*if recipe.isFavorite{
            cell.accessoryType = .checkmark //coloca un simbolo de selecciona a la celda seleccionada
        }else{
            cell.accessoryType = .none  //por manejo del dequeueReusableCell reutilizacion de celdas en la vista
        }*/
        
        return cell
    }
    
    //Editar modificar celdas de la TableView : administrar las celdas por defecto
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {  //Edicion de una celda commit : Vista
        
        if editingStyle == .delete{
            self.recipes.remove(at: indexPath.row)  //Elimina elemento del array actual : Modelo
        }
        
        self.tableView.deleteRows(at: [indexPath], with: .fade) //Actualizar la tabla solo la fila borrada
    }

    //Agregar acciones perzonalizadas swape (deslizar celda) : administracion de las celdas personalizadas
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? { //Sobre escribimos commit editinStyle
        
        //Action de compartir al deslizar la celda
        let shareAction = UITableViewRowAction(style: .default, title: "Compartir") { (action, indexPath) in
            
            let shareDefaultText = "Estoy mirando la receta de \(self.recipes[indexPath.row].name!) en la app de Recetas de Cocina"  //Titulo  que se mostrara en la activityController
            let activityController = UIActivityViewController(activityItems: [shareDefaultText, self.recipes[indexPath.row].image], applicationActivities: nil)//UIActivityViewController muestra varios servicios redes sociales, publicar
            self.present(activityController, animated: true, completion: nil)                                                           //Se muetra activityController en la propia vista
        }
        shareAction.backgroundColor = UIColor(red: 30.0/255.5, green: 164.0/255.0, blue: 253.0/255.0, alpha: 1.0) //modificar color shareAction
        
        //Accion de borrar
        let deleteAction = UITableViewRowAction(style: .default, title: "Borrar") { (action, indexPath) in
            self.recipes.remove(at: indexPath.row) //Eliminamos del array el objeto seleccinado : Modelo
            self.tableView.deleteRows(at: [indexPath], with: .fade) //Elimina de la vista la celda del indexPath actual
        }
        deleteAction.backgroundColor = UIColor(red: 202.0/255.0, green: 202.0/255.0, blue: 202.0/255.0, alpha: 1.0) //Color deleteAction
        
        return [shareAction, deleteAction]
    }
    
    
    
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) { //Se ejecuta cuando se selecciona una celda de la tabla
        
        /*print("He seleccionado la fila \(indexPath.row)") //mostrara la fila seleccinada
        
        let recipe = recipes[indexPath.row] //Recuperamos la celda actual y obtener sus atributos
        
        let alertController = UIAlertController(title: recipe.name, message: "Valora este plato", preferredStyle: .alert) //Creamos la alerta Title: nombre de la fila actual
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil) //Creamos el boton que tendra la alerta
        
        var favoriteActionTitle = "Favorito"  //Titulo de favoriteAction que se muestra cuando se va a seleccionar la celda
        var favoriteActionStyle = UIAlertActionStyle.default  //Estilo de la accion favoriteAction
        
        if recipe.isFavorite {
            favoriteActionTitle = "No Favorito"
            favoriteActionStyle = UIAlertActionStyle.destructive
        }

        let favoriteAction = UIAlertAction(title: favoriteActionTitle, style: favoriteActionStyle) { (action) in  //Se ejecuta al darle al boton de favorito : es un bloque de codigo
            let recipe = self.recipes[indexPath.row] //Recuperamos la fila seleccionada
            recipe.isFavorite = !recipe.isFavorite //cambiamos el estado de la receta del atributo isFAvorite
            
            self.tableView.reloadData() //Actualizamos la tabla para que nos colo el checkmark
        }
        alertController.addAction(favoriteAction) //Agregamos la accion favoriteAction
        alertController.addAction(cancelAction) //Agregamos la accion al alerController (boton)
        self.present(alertController, animated: true, completion: nil) //Indicamos que se mostrara el alertController a la propia vista*/
    }
    
    
    // Pasar informacion de una vista a otro a traves del segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRecipeDetail"{ //verificamos el segue preparado o indicado Vista
            
            if let indexPath = self.tableView.indexPathForSelectedRow{ // Fila seleccinaoda por medio del indexPath : Celda
                let selectedRecipe = self.recipes[indexPath.row]  //obtenemos el objeto del array  del modelo Recipe : La receta de la posicion seleccinada
                
                let destinatedViewController = segue.destination  as! DetailViewController //ViewControler de destino casting al ViewController que se enviaran los datos
                destinatedViewController.recipe = selectedRecipe    //Accedemos al objeto Recipe de DetailViewController le asignamos el objeto Recipe seleccionado
            
            }
        
        }
        
    }
    
    
}

